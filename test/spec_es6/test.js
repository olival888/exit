/* global describe, it */

'use strict';

(() => {
    describe('Object ImgLoad', () => {
	    let Modernizr;

	    let imgElem=document.createElement("img");
	    let rootFolder="/images/media/quests/index";

	    let img, imgLocal, imgGroup;

	    before(() => {
		    imgElem.id="img";
		    imgElem.src="";
		    imgElem.dataset.src=1;

		    img=new ImgLoad(imgElem,rootFolder);
		    imgLocal=new ImgLoad(imgElem,rootFolder,"local");

		    Modernizr={
			    blobconstructor:true,
			    bloburls:true,
			    filereader:true
		    };
	    });
	    after(() => {
		    img=imgLocal={};
		    imgElem.src="";
	    });

	    beforeEach(function() {

	    });
	    afterEach(function() {
		    /*localforage.clear();*/
	    });
        describe('loading alone', () => {
	        describe('test SRC generate', () => {
		        it(`with real screenwidth - ${window.screen.width} and screen ratio - ${window.devicePixelRatio}`, () => {
			        function makeUrl(fileName, folder) {
				        let getFormat = ()=>{
					        if(document.documentElement.classList.contains("webp")){
						        return "webp";
					        }else if(Modernizr.jp2){
						        return "jp2";
					        }else{
						        return "jpg";
					        }
				        };

				        let dpr = window.devicePixelRatio >= 1.5 ? "2x" : "1x";
				        let resolutionFolder = (screenWidth)=>{
					        if (screenWidth > 3000) {
						        return "4000";
					        } else if (screenWidth > 2500) {
						        return "3000";
					        } else if (screenWidth > 2000) {
						        return "2500";
					        } else if (screenWidth > 1500) {
						        return "2000";
					        } else {
						        return "1500";
					        }
				        };

				        return `${folder}/${getFormat()}/${dpr}/${resolutionFolder(window.screen.width)}/${fileName}.${getFormat()}`;
			        }

			        assert.equal(img.src,makeUrl(imgElem.dataset.src, rootFolder));
		        });
	        });

	        describe('test loadImage method', () => {
		        describe('without storage', () => {
			        it("whith real and exist arguments",()=>{
				        //img.loadImage().should.be.reject;
			        });
		        });
		        describe('with localforage storage', () => {
			        it("whith real and exist arguments",()=>{
				        //imgLocal.loadImage().should.be.reject;
			        });
		        });
	        });

	        describe('test getBlobImg method', () => {
		        it("whith real and exist arguments and clear localforage",()=>{
			        /*localforage.clear();
			        img.getBlobImg().should.be.fulfilled;*/
		        });
		        it("whith real and exist arguments and not clear localforage",()=>{
			        //img.getBlobImg().should.not.be.reject;
		        });
	        });

	        describe("test fillSrc method",()=>{
		        it("whith real and exist arguments",()=>{

		        });
	        });


	        /*используя шпиона, вывести результаты для других значений ширины и плотности*/
	        /*используя шпиона, вывести результаты для других значений всего*/
        });

	    describe('loading group', () => {
		    describe('test loadImages', () => {
			    it("whith real and exist arguments",()=>{
				    /*
				    * протестить, что грузятся, что по порядку грузятся
				    * */
			    });
		    });
	    });
    });
})();
