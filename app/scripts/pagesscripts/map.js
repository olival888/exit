"use strict";

var map;
function init() {
	var mapCenter=new google.maps.LatLng(55.751900, 37.587222);

	var mapOptions = {
		center: mapCenter,
		zoom:16,
		minZoom: 12,
		maxZoom:16,
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		},
		disableDoubleClickZoom: true,
		mapTypeControl: false,
		scaleControl: false,
		scrollwheel: true,
		panControl: false,
		streetViewControl: false,
		draggable: true,
		overviewMapControl: false,
		overviewMapControlOptions: {
			opened: false
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		styles: [
			{ stylers: [
				{ saturation: -100 },
				{ gamma: 1 }
			] },
			{ elementType: "labels.text.stroke", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "poi.business", elementType: "labels.text", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "poi.business", elementType: "labels.icon", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "poi.place_of_worship", elementType: "labels.text", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "poi.place_of_worship", elementType: "labels.icon", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "road", elementType: "geometry", stylers: [
				{ visibility: "simplified" }
			] },
			{ featureType: "water", stylers: [
				{ visibility: "on" },
				{ saturation: 50 },
				{ gamma: 0 },
				{ hue: "#50a5d1" }
			] },
			{ featureType: "administrative.neighborhood", elementType: "labels.text.fill", stylers: [
				{ color: "#333333" }
			] },
			{ featureType: "road.local", elementType: "labels.text", stylers: [
				{ weight: 0.5 },
				{ color: "#333333" }
			] },
			{ featureType: "transit.station", elementType: "labels.icon", stylers: [
				{ gamma: 1 },
				{ saturation: 50 }
			] }
		]
	};

	var map = new google.maps.Map(document.getElementById('map'), mapOptions);

	var marker = new google.maps.Marker({
		position: map.getCenter(),
		map: map,
		title: 'EXIT',
		icon:'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxMDAgMTAwIj48cGF0aCBkPSJNNDkuMyA0LjhjLTE0LjMgMC0yNiAxMS42LTI2IDI2IDAgNS44IDEuOSAxMS4yIDUuMiAxNS42LjYuOSAyMC41IDMwLjEgMjAuOCA0OS40LjMtMTkuNCAyMC4xLTQ4LjUgMjAuOC00OS40IDMuMi00LjMgNS4yLTkuNyA1LjItMTUuNiAwLTE0LjQtMTEuNi0yNi0yNi0yNnoiLz48L3N2Zz4='
	});

	document.addEventListener("click",function(e){
		if(e.target && e.target.nodeName == "LABEL" && e.target.className == "views-list__label"){
			map.panTo(mapCenter);
			map.setZoom(16);
		}
	});

	google.maps.event.addListener(marker, 'click', function(event) {
		window.open('https://goo.gl/maps/8buki')
	});




	var myPanoid = "QKZJDF5QWO0AAAAAAAABOw";
	var panoramaOptions = {
		pano: myPanoid,
		pov: {
			heading: 45,
			pitch:-2
		},
		zoom: 1
	};
	var myPhotoSphere = new google.maps.StreetViewPanorama(
		document.getElementById('sphere'),
		panoramaOptions);
	myPano.setVisible(true);
}

google.maps.event.addDomListener(window, 'load', init);
