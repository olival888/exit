"use strict";

Modernizr.addTest('woff2', () => {
	if( !( "FontFace" in window ) ) {
		return false;
	}else{
		let f = new FontFace('t', 'url("data:application/font-woff2,") format("woff2")', {});

		try {
			f.load().catch(function() {});

			return f.status == 'loading';
		}catch(e) {
			throw e;
		}
	}
});