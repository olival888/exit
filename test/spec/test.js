/* global describe, it */

'use strict';

(function () {
	describe('Object ImgLoad', function () {
		describe('loading alone', function () {
			var imgElem = document.getElementById("img");
			var rootFolder = "/images/media/quests/index";

			var img = undefined,
			    imgLocal = undefined,
			    imgGroup = undefined;

			before(function () {
				img = new ImgLoad(imgElem, rootFolder);
				imgLocal = new ImgLoad(imgElem, rootFolder, "local");
			});
			after(function () {
				img = imgLocal = {};
				imgElem.src = "";
			});

			beforeEach(function () {});
			afterEach(function () {
				/*localforage.clear();*/
			});

			describe('test SRC generate', function () {
				it('with real screenwidth - ' + window.screen.width + ' and screen ratio - ' + window.devicePixelRatio, function () {
					function makeUrl(fileName, folder) {
						var getFormat = function getFormat() {
							if (document.documentElement.classList.contains("webp")) {
								return "webp";
							} else if (Modernizr.jp2) {
								return "jp2";
							} else {
								return "jpg";
							}
						};

						var dpr = window.devicePixelRatio >= 1.5 ? "2x" : "1x";
						var resolutionFolder = function resolutionFolder(screenWidth) {
							if (screenWidth > 3000) {
								return "4000";
							} else if (screenWidth > 2500) {
								return "3000";
							} else if (screenWidth > 2000) {
								return "2500";
							} else if (screenWidth > 1500) {
								return "2000";
							} else {
								return "1500";
							}
						};

						return folder + '/' + getFormat() + '/' + dpr + '/' + resolutionFolder(window.screen.width) + '/' + fileName + '.' + getFormat();
					}

					assert.equal(img.src, makeUrl(imgElem.dataset.src, rootFolder));
				});
			});

			describe('test loadImage method', function () {
				describe('without storage', function () {
					it("whith real and exist arguments", function () {
						//img.loadImage().should.be.reject;
					});
				});
				describe('with localforage storage', function () {
					it("whith real and exist arguments", function () {
						//imgLocal.loadImage().should.be.reject;
					});
				});
			});

			describe('test getBlobImg method', function () {
				it("whith real and exist arguments and clear localforage", function () {
					/*localforage.clear();
     img.getBlobImg().should.be.fulfilled;*/
				});
				it("whith real and exist arguments and not clear localforage", function () {
					//img.getBlobImg().should.not.be.reject;
				});
			});

			describe("test fillSrc method", function () {
				it("whith real and exist arguments", function () {});
			});

			/*используя шпиона, вывести результаты для других значений ширины и плотности*/
			/*используя шпиона, вывести результаты для других значений всего*/
		});

		describe('loading group', function () {
			describe('test loadImages', function () {
				it("whith real and exist arguments", function () {});
			});
		});
	});
})();
//# sourceMappingURL=test.js.map