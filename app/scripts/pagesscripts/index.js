"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	/*let topImg = new ImgLoad(document.querySelector(".top__img"), "/images/media/main-img", "local");
	topImg.loadImage()
		.then(()=> {
			loadImages(document.querySelectorAll(".quest__img[data-src]"), "/images/media/quests/index");
		});*/
	/*var imgArr=document.querySelectorAll(".quest__img[data-src]");
	if(!imgArr.length%2){
		loadImages(imgArr, "/images/media/quests/index");
	}else{
		loadImages(document.querySelectorAll(".quest__img[data-src]"), "/images/media/quests/index")
	}*/

	Array.from(document.querySelectorAll(".set")).forEach((set)=>{
		let imgArr=set.querySelectorAll(".quest__img[data-src]");

		/*/media/desktop_version/images/quests*/
		if(!(imgArr.length%2)){
			loadImages(imgArr, "/images/media/quests/index","none",5000);
		}else{
			loadImages(set.querySelectorAll(".quests .quest:not(:last-child) .quest__img[data-src]"), "/images/media/quests/index")
				.then(()=>{(new ImgLoad(set.querySelector(".quests .quest:last-child .quest__img[data-src]"), "/images/media/quests/full")).loadImage()});
		}
	});
});
