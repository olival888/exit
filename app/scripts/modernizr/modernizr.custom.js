/*!
 * modernizr v3.0.0-alpha.3
 * Build http://v3.modernizr.com/download/#-apng-blobconstructor-bloburls-classlist-csscalc-cssfilters-cssremunit-cssvhunit-cssvwunit-customevent-dataset-datauri-es5syntax-es6array-es6string-eventlistener-filereader-flexbox-flexboxlegacy-flexboxtweener-formvalidation-generators-hidden-hiddenscroll-ie8compat-inlinesvg-jpegxr-promises-scriptasync-scriptdefer-svgfilters-video-webp-xhrresponsetypearraybuffer-xhrresponsetypeblob-xhrresponsetypetext-addtest-hasevent-mq-testallprops-testprop-teststyles-dontmin
 *
 * Copyright (c)
 *  Faruk Ates
 *  Paul Irish
 *  Alex Sexton
 *  Ryan Seddon
 *  Alexander Farkas
 *  Patrick Kettner
 *  Stu Cox
 *  Richard Herrera

 * MIT License
 */

/*
 * Modernizr tests which native CSS3 and HTML5 features are available in the
 * current UA and makes the results available to you in two ways: as properties on
 * a global `Modernizr` object, and as classes on the `<html>` element. This
 * information allows you to progressively enhance your pages with a granular level
 * of control over the experience.
 */

;(function(window, document, undefined){
	var classes = [];


	var tests = [];


	var ModernizrProto = {
		// The current version, dummy
		_version: '3.0.0-alpha.3',

		// Any settings that don't work as separate modules
		// can go in here as configuration.
		_config: {
			'classPrefix' : '',
			'enableClasses' : true,
			'enableJSClass' : true,
			'usePrefixes' : true
		},

		// Queue of tests
		_q: [],

		// Stub these for people who are listening
		on: function( test, cb ) {
			// I don't really think people should do this, but we can
			// safe guard it a bit.
			// -- NOTE:: this gets WAY overridden in src/addTest for
			// actual async tests. This is in case people listen to
			// synchronous tests. I would leave it out, but the code
			// to *disallow* sync tests in the real version of this
			// function is actually larger than this.
			var self = this;
			setTimeout(function() {
				cb(self[test]);
			}, 0);
		},

		addTest: function( name, fn, options ) {
			tests.push({name : name, fn : fn, options : options });
		},

		addAsyncTest: function (fn) {
			tests.push({name : null, fn : fn});
		}
	};



	// Fake some of Object.create
	// so we can force non test results
	// to be non "own" properties.
	var Modernizr = function(){};
	Modernizr.prototype = ModernizrProto;

	// Leak modernizr globally when you `require` it
	// rather than force it here.
	// Overwrite name so constructor name is nicer :D
	Modernizr = new Modernizr();


	/*!
	 {
	 "name": "Blob constructor",
	 "property": "blobconstructor",
	 "aliases": ["blob-constructor"],
	 "builderAliases": ["blob_constructor"],
	 "caniuse": "blobbuilder",
	 "notes": [{
	 "name": "W3C spec",
	 "href": "http://dev.w3.org/2006/webapi/FileAPI/#constructorBlob"
	 }],
	 "polyfills": ["blobjs"]
	 }
	 !*/
	/* DOC
	 Detects support for the Blob constructor, for creating file-like objects of immutable, raw data.
	 */

	Modernizr.addTest('blobconstructor', function () {
		try {
			return !!new Blob();
		} catch (e) {
			return false;
		}
	}, {
		aliases: ['blob-constructor']
	});

	/*!
	 {
	 "name": "CustomEvent",
	 "property": "customevent",
	 "tags": ["customevent"],
	 "authors": ["Alberto Elias"],
	 "notes": [{
	 "name": "W3C DOM reference",
	 "href": "http://www.w3.org/TR/DOM-Level-3-Events/#interface-CustomEvent"
	 }, {
	 "name": "MDN documentation",
	 "href": "https://developer.mozilla.org/en/docs/Web/API/CustomEvent"
	 }],
	 "polyfills": ["eventlistener"]
	 }
	 !*/
	/* DOC

	 Detects support for CustomEvent.

	 */

	Modernizr.addTest('customevent', 'CustomEvent' in window && typeof window.CustomEvent === 'function');

	/*!
	 {
	 "name": "ES5 Syntax",
	 "property": "es5syntax",
	 "notes": [{
	 "name": "ECMAScript 5.1 Language Specification",
	 "href": "http://www.ecma-international.org/ecma-262/5.1/"
	 }, {
	 "name": "original implementation of detect code",
	 "href": "http://kangax.github.io/es5-compat-table/"
	 }],
	 "authors": ["Ron Waldon (@jokeyrhyme)"],
	 "warnings": ["This detect uses `eval()`, so CSP may be a problem."],
	 "tags": ["es5"]
	 }
	 !*/
	/* DOC
	 Check if browser accepts ECMAScript 5 syntax.
	 */

	Modernizr.addTest('es5syntax', function () {
		var value, obj, stringAccess, getter, setter, reservedWords, zeroWidthChars;
		try {
			// Property access on strings
			stringAccess = eval('"foobar"[3] === "b"');
			// Getter in property initializer
			getter = eval('({ get x(){ return 1 } }).x === 1');
			eval('({ set x(v){ value = v; } }).x = 1');
			// Setter in property initializer
			setter = value === 1;
			// Reserved words as property names
			eval('obj = ({ if: 1 })');
			reservedWords = obj['if'] === 1;
			// Zero-width characters in identifiers
			zeroWidthChars = eval('_\u200c\u200d = true');

			return stringAccess && getter && setter && reservedWords && zeroWidthChars;
		} catch (ignore) {
			return false;
		}
	});

	/*!
	 {
	 "name": "ES6 Array",
	 "property": "es6array",
	 "notes": [{
	 "name": "unofficial ECMAScript 6 draft specification",
	 "href": "http://people.mozilla.org/~jorendorff/es6-draft.html"
	 }],
	 "polyfills": ["es6shim"],
	 "authors": ["Ron Waldon (@jokeyrhyme)"],
	 "warnings": ["ECMAScript 6 is still a only a draft, so this detect may not match the final specification or implementations."],
	 "tags": ["es6"]
	 }
	 !*/
	/* DOC
	 Check if browser implements ECMAScript 6 Array per specification.
	 */

	Modernizr.addTest('es6array', !!(Array.prototype &&
	Array.prototype.copyWithin &&
	Array.prototype.fill &&
	Array.prototype.find &&
	Array.prototype.findIndex &&
	Array.prototype.keys &&
	Array.prototype.entries &&
	Array.prototype.values &&
	Array.from &&
	Array.of));

	/*!
	 {
	 "name": "ES6 Generators",
	 "property": "generators",
	 "authors": ["Michael Kachanovskyi"],
	 "tags": ["es6"]
	 }
	 !*/
	/* DOC
	 Check if browser implements ECMAScript 6 Generators per specification.
	 */

	Modernizr.addTest('generators', function() {
		try {
			/* jshint evil: true */
			new Function('function* test() {}')();
		} catch(e) {
			return false;
		}
		return true;
	});

	/*!
	 {
	 "name": "ES6 Promises",
	 "property": "promises",
	 "caniuse": "promises",
	 "polyfills": ["es6promises"],
	 "authors": ["Krister Kari", "Jake Archibald"],
	 "tags": ["es6"],
	 "notes": [{
	 "name": "The ES6 promises spec",
	 "href": "https://github.com/domenic/promises-unwrapping"
	 },{
	 "name": "Chromium dashboard - ES6 Promises",
	 "href": "http://www.chromestatus.com/features/5681726336532480"
	 },{
	 "name": "JavaScript Promises: There and back again - HTML5 Rocks",
	 "href": "http://www.html5rocks.com/en/tutorials/es6/promises/"
	 }]
	 }
	 !*/
	/* DOC
	 Check if browser implements ECMAScript 6 Promises per specification.
	 */

	Modernizr.addTest('promises', function() {
		return 'Promise' in window &&
				// Some of these methods are missing from
				// Firefox/Chrome experimental implementations
			'resolve' in window.Promise &&
			'reject' in window.Promise &&
			'all' in window.Promise &&
			'race' in window.Promise &&
				// Older version of the spec had a resolver object
				// as the arg rather than a function
			(function() {
				var resolve;
				new window.Promise(function(r) { resolve = r; });
				return typeof resolve === 'function';
			}());
	});

	/*!
	 {
	 "name": "ES6 String",
	 "property": "es6string",
	 "notes": [{
	 "name": "unofficial ECMAScript 6 draft specification",
	 "href": "http://people.mozilla.org/~jorendorff/es6-draft.html"
	 }],
	 "polyfills": ["es6shim"],
	 "authors": ["Ron Waldon (@jokeyrhyme)"],
	 "warnings": ["ECMAScript 6 is still a only a draft, so this detect may not match the final specification or implementations."],
	 "tags": ["es6"]
	 }
	 !*/
	/* DOC
	 Check if browser implements ECMAScript 6 String per specification.
	 */

	Modernizr.addTest('es6string', !!(String.fromCodePoint &&
	String.raw &&
	String.prototype.codePointAt &&
	String.prototype.repeat &&
	String.prototype.startsWith &&
	String.prototype.endsWith &&
	String.prototype.contains));

	/*!
	 {
	 "name": "Event Listener",
	 "property": "eventlistener",
	 "authors": ["Andrew Betts (@triblondon)"],
	 "notes": [{
	 "name": "W3C Spec",
	 "href": "http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-Registration-interfaces"
	 }],
	 "polyfills": ["eventlistener"]
	 }
	 !*/
	/* DOC
	 Detects native support for addEventListener
	 */

	Modernizr.addTest('eventlistener', 'addEventListener' in window);

	/*!
	 {
	 "name": "File API",
	 "property": "filereader",
	 "caniuse": "fileapi",
	 "notes": [{
	 "name": "W3C Working Draft",
	 "href": "http://www.w3.org/TR/FileAPI/"
	 }],
	 "tags": ["file"],
	 "builderAliases": ["file_api"],
	 "knownBugs": ["Will fail in Safari 5 due to its lack of support for the standards defined FileReader object"]
	 }
	 !*/
	/* DOC
	 `filereader` tests for the File API specification

	 Tests for objects specific to the File API W3C specification without
	 being redundant (don't bother testing for Blob since it is assumed
	 to be the File object's prototype.)
	 */

	Modernizr.addTest('filereader', !!(window.File && window.FileList && window.FileReader));

	/*!
	 {
	 "name": "IE8 compat mode",
	 "property": "ie8compat",
	 "authors": ["Erich Ocean"]
	 }
	 !*/
	/* DOC
	 Detects whether or not the current browser is IE8 in compatibility mode (i.e. acting as IE7).
	 */

	// In this case, IE8 will be acting as IE7. You may choose to remove features in this case.

	// related:
	// james.padolsey.com/javascript/detect-ie-in-js-using-conditional-comments/

	Modernizr.addTest('ie8compat', (!window.addEventListener && !!document.documentMode && document.documentMode === 7));

	/*!
	 {
	 "name": "SVG filters",
	 "property": "svgfilters",
	 "caniuse": "svg-filters",
	 "tags": ["svg"],
	 "builderAliases": ["svg_filters"],
	 "authors": ["Erik Dahlstrom"],
	 "notes": [{
	 "name": "W3C Spec",
	 "href": "http://www.w3.org/TR/SVG11/filters.html"
	 }]
	 }
	 !*/

	// Should fail in Safari: http://stackoverflow.com/questions/9739955/feature-detecting-support-for-svg-filters.
	Modernizr.addTest('svgfilters', function() {
		var result = false;
		try {
			result = 'SVGFEColorMatrixElement' in window &&
				SVGFEColorMatrixElement.SVG_FECOLORMATRIX_TYPE_SATURATE == 2;
		}
		catch(e) {}
		return result;
	});


	/**
	 * is returns a boolean for if typeof obj is exactly type.
	 */
	function is( obj, type ) {
		return typeof obj === type;
	}
	;

	// Run through all tests and detect their support in the current UA.
	function testRunner() {
		var featureNames;
		var feature;
		var aliasIdx;
		var result;
		var nameIdx;
		var featureName;
		var featureNameSplit;

		for ( var featureIdx in tests ) {
			featureNames = [];
			feature = tests[featureIdx];
			// run the test, throw the return value into the Modernizr,
			//   then based on that boolean, define an appropriate className
			//   and push it into an array of classes we'll join later.
			//
			//   If there is no name, it's an 'async' test that is run,
			//   but not directly added to the object. That should
			//   be done with a post-run addTest call.
			if ( feature.name ) {
				featureNames.push(feature.name.toLowerCase());

				if (feature.options && feature.options.aliases && feature.options.aliases.length) {
					// Add all the aliases into the names list
					for (aliasIdx = 0; aliasIdx < feature.options.aliases.length; aliasIdx++) {
						featureNames.push(feature.options.aliases[aliasIdx].toLowerCase());
					}
				}
			}

			// Run the test, or use the raw value if it's not a function
			result = is(feature.fn, 'function') ? feature.fn() : feature.fn;


			// Set each of the names on the Modernizr object
			for (nameIdx = 0; nameIdx < featureNames.length; nameIdx++) {
				featureName = featureNames[nameIdx];
				// Support dot properties as sub tests. We don't do checking to make sure
				// that the implied parent tests have been added. You must call them in
				// order (either in the test, or make the parent test a dependency).
				//
				// Cap it to TWO to make the logic simple and because who needs that kind of subtesting
				// hashtag famous last words
				featureNameSplit = featureName.split('.');

				if (featureNameSplit.length === 1) {
					Modernizr[featureNameSplit[0]] = result;
				} else {
					// cast to a Boolean, if not one already
					/* jshint -W053 */
					if (Modernizr[featureNameSplit[0]] && !(Modernizr[featureNameSplit[0]] instanceof Boolean)) {
						Modernizr[featureNameSplit[0]] = new Boolean(Modernizr[featureNameSplit[0]]);
					}

					Modernizr[featureNameSplit[0]][featureNameSplit[1]] = result;
				}

				classes.push((result ? '' : 'no-') + featureNameSplit.join('-'));
			}
		}
	}

	;

	var docElement = document.documentElement;


	// Pass in an and array of class names, e.g.:
	//  ['no-webp', 'borderradius', ...]
	function setClasses( classes ) {
		var className = docElement.className;
		var classPrefix = Modernizr._config.classPrefix || '';

		// Change `no-js` to `js` (we do this independently of the `enableClasses`
		// option)
		// Handle classPrefix on this too
		if(Modernizr._config.enableJSClass) {
			var reJS = new RegExp('(^|\\s)'+classPrefix+'no-js(\\s|$)');
			className = className.replace(reJS, '$1'+classPrefix+'js$2');
		}

		if(Modernizr._config.enableClasses) {
			// Add the new classes
			className += ' ' + classPrefix + classes.join(' ' + classPrefix);
			docElement.className = className;
		}

	}

	;
	/*!
	 {
	 "name": "classList",
	 "caniuse": "classlist",
	 "property": "classlist",
	 "tags": ["dom"],
	 "builderAliases": ["dataview_api"],
	 "notes": [{
	 "name": "MDN Docs",
	 "href": "https://developer.mozilla.org/en/DOM/element.classList"
	 }]
	 }
	 !*/

	Modernizr.addTest('classlist', 'classList' in docElement);


	// hasOwnProperty shim by kangax needed for Safari 2.0 support
	var hasOwnProp;

	(function() {
		var _hasOwnProperty = ({}).hasOwnProperty;
		/* istanbul ignore else */
		/* we have no way of testing IE 5.5 or safari 2,
		 * so just assume the else gets hit */
		if ( !is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined') ) {
			hasOwnProp = function (object, property) {
				return _hasOwnProperty.call(object, property);
			};
		}
		else {
			hasOwnProp = function (object, property) { /* yes, this can give false positives/negatives, but most of the time we don't care about those */
				return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
			};
		}
	})();



	// As far as I can think of, we shouldn't need or
	// allow 'on' for non-async tests, and you can't do
	// async tests without this 'addTest' module.

	// Listeners for async or post-run tests
	ModernizrProto._l = {};

	// 'addTest' implies a test after the core runloop,
	// So we'll add in the events
	ModernizrProto.on = function( test, cb ) {
		// Create the list of listeners if it doesn't exist
		if (!this._l[test]) {
			this._l[test] = [];
		}

		// Push this test on to the listener list
		this._l[test].push(cb);

		// If it's already been resolved, trigger it on next tick
		if (Modernizr.hasOwnProperty(test)) {
			// Next Tick
			setTimeout(function() {
				Modernizr._trigger(test, Modernizr[test]);
			}, 0);
		}
	};

	ModernizrProto._trigger = function( test, res ) {
		if (!this._l[test]) {
			return;
		}

		var cbs = this._l[test];

		// Force async
		setTimeout(function() {
			var i, cb;
			for (i = 0; i < cbs.length; i++) {
				cb = cbs[i];
				cb(res);
			}
		},0);

		// Don't trigger these again
		delete this._l[test];
	};

	/**
	 * addTest allows the user to define their own feature tests
	 * the result will be added onto the Modernizr object,
	 * as well as an appropriate className set on the html element
	 *
	 * @param feature - String naming the feature
	 * @param test - Function returning true if feature is supported, false if not
	 */
	function addTest( feature, test ) {
		if ( typeof feature == 'object' ) {
			for ( var key in feature ) {
				if ( hasOwnProp( feature, key ) ) {
					addTest( key, feature[ key ] );
				}
			}
		} else {

			feature = feature.toLowerCase();
			var featureNameSplit = feature.split('.');
			var last = Modernizr[featureNameSplit[0]];

			// Again, we don't check for parent test existence. Get that right, though.
			if (featureNameSplit.length == 2) {
				last = last[featureNameSplit[1]];
			}

			if ( typeof last != 'undefined' ) {
				// we're going to quit if you're trying to overwrite an existing test
				// if we were to allow it, we'd do this:
				//   var re = new RegExp("\\b(no-)?" + feature + "\\b");
				//   docElement.className = docElement.className.replace( re, '' );
				// but, no rly, stuff 'em.
				return Modernizr;
			}

			test = typeof test == 'function' ? test() : test;

			// Set the value (this is the magic, right here).
			if (featureNameSplit.length == 1) {
				Modernizr[featureNameSplit[0]] = test;
			} else {
				// cast to a Boolean, if not one already
				/* jshint -W053 */
				if (Modernizr[featureNameSplit[0]] && !(Modernizr[featureNameSplit[0]] instanceof Boolean)) {
					Modernizr[featureNameSplit[0]] = new Boolean(Modernizr[featureNameSplit[0]]);
				}

				Modernizr[featureNameSplit[0]][featureNameSplit[1]] = test;
			}

			// Set a single class (either `feature` or `no-feature`)
			/* jshint -W041 */
			setClasses([(!!test && test != false ? '' : 'no-') + featureNameSplit.join('-')]);
			/* jshint +W041 */

			// Trigger the event
			Modernizr._trigger(feature, test);
		}

		return Modernizr; // allow chaining.
	}

	// After all the tests are run, add self
	// to the Modernizr prototype
	Modernizr._q.push(function() {
		ModernizrProto.addTest = addTest;
	});


	/*!
	 {
	 "name": "Data URI",
	 "property": "datauri",
	 "caniuse": "datauri",
	 "tags": ["url"],
	 "builderAliases": ["url_data_uri"],
	 "async": true,
	 "notes": [{
	 "name": "Wikipedia article",
	 "href": "http://en.wikipedia.org/wiki/Data_URI_scheme"
	 }],
	 "warnings": ["Support in Internet Explorer 8 is limited to images and linked resources like CSS files, not HTML files"]
	 }
	 !*/
	/* DOC
	 Detects support for data URIs. Provides a subproperty to report support for data URIs over 32kb in size:

	 ```javascript
	 Modernizr.datauri           // true
	 Modernizr.datauri.over32kb  // false in IE8
	 ```
	 */

	// https://github.com/Modernizr/Modernizr/issues/14
	Modernizr.addAsyncTest(function() {
		/* jshint -W053 */

		// IE7 throw a mixed content warning on HTTPS for this test, so we'll
		// just blacklist it (we know it doesn't support data URIs anyway)
		// https://github.com/Modernizr/Modernizr/issues/362
		if(navigator.userAgent.indexOf('MSIE 7.') !== -1) {
			// Keep the test async
			setTimeout(function () {
				addTest('datauri', false);
			}, 10);
		}

		var datauri = new Image();

		datauri.onerror = function() {
			addTest('datauri', false);
		};
		datauri.onload = function() {
			if(datauri.width == 1 && datauri.height == 1) {
				testOver32kb();
			}
			else {
				addTest('datauri', false);
			}
		};

		datauri.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

		// Once we have datauri, let's check to see if we can use data URIs over
		// 32kb (IE8 can't). https://github.com/Modernizr/Modernizr/issues/321
		function testOver32kb(){

			var datauriBig = new Image();

			datauriBig.onerror = function() {
				addTest('datauri', true);
				Modernizr.datauri = new Boolean(true);
				Modernizr.datauri.over32kb = false;
			};
			datauriBig.onload = function() {
				addTest('datauri', true);
				Modernizr.datauri = new Boolean(true);
				Modernizr.datauri.over32kb = (datauriBig.width == 1 && datauriBig.height == 1);
			};

			var base64str = 'R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';
			while (base64str.length < 33000) {
				base64str = '\r\n' + base64str;
			}
			datauriBig.src= 'data:image/gif;base64,' + base64str;
		}

	});

	/*!
	 {
	 "name": "JPEG XR (extended range)",
	 "async": true,
	 "aliases": ["jpeg-xr"],
	 "property": "jpegxr",
	 "tags": ["image"],
	 "notes": [{
	 "name": "Wikipedia Article",
	 "href": "http://en.wikipedia.org/wiki/JPEG_XR"
	 }]
	 }
	 !*/
	/* DOC
	 Test for JPEG XR support
	 */


	Modernizr.addAsyncTest(function() {
		var image = new Image();

		image.onload = image.onerror = function() {
			addTest('jpegxr', image.width == 1, { aliases: ['jpeg-xr'] });
		};

		image.src = 'data:image/vnd.ms-photo;base64,SUm8AQgAAAAFAAG8AQAQAAAASgAAAIC8BAABAAAAAQAAAIG8BAABAAAAAQAAAMC8BAABAAAAWgAAAMG8BAABAAAAHwAAAAAAAAAkw91vA07+S7GFPXd2jckNV01QSE9UTwAZAYBxAAAAABP/gAAEb/8AAQAAAQAAAA==';
	});

	/*!
	 {
	 "name": "Webp",
	 "async": true,
	 "property": "webp",
	 "tags": ["image"],
	 "builderAliases": ["img_webp"],
	 "authors": ["Krister Kari", "@amandeep", "Rich Bradshaw", "Ryan Seddon", "Paul Irish"],
	 "notes": [{
	 "name": "Webp Info",
	 "href": "http://code.google.com/speed/webp/"
	 }, {
	 "name": "Chormium blog - Chrome 32 Beta: Animated WebP images and faster Chrome for Android touch input",
	 "href": "http://blog.chromium.org/2013/11/chrome-32-beta-animated-webp-images-and.html"
	 }, {
	 "name": "Webp Lossless Spec",
	 "href": "https://developers.google.com/speed/webp/docs/webp_lossless_bitstream_specification"
	 }, {
	 "name": "Article about WebP support on Android browsers",
	 "href": "http://www.wope-framework.com/en/2013/06/24/webp-support-on-android-browsers/"
	 }, {
	 "name": "Chormium WebP announcement",
	 "href": "http://blog.chromium.org/2011/11/lossless-and-transparency-encoding-in.html?m=1"
	 }]
	 }
	 !*/
	/* DOC
	 Tests for lossy, non-alpha webp support.
	 =======

	 Tests for all forms of webp support (lossless, lossy, alpha, and animated)..

	 Modernizr.webp              // Basic support (lossy)
	 Modernizr.webp.lossless     // Lossless
	 Modernizr.webp.alpha        // Alpha (both lossy and lossless)
	 Modernizr.webp.animation    // Animated WebP

	 */


	Modernizr.addAsyncTest(function() {

		var webpTests = [{
			'uri': 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=',
			'name': 'webp'
		},{
			'uri': 'data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA==',
			'name': 'webp.alpha'
		},{
			'uri': 'data:image/webp;base64,UklGRlIAAABXRUJQVlA4WAoAAAASAAAAAAAAAAAAQU5JTQYAAAD/////AABBTk1GJgAAAAAAAAAAAAAAAAAAAGQAAABWUDhMDQAAAC8AAAAQBxAREYiI/gcA',
			'name': 'webp.animation'
		}, {
			'uri': 'data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=',
			'name': 'webp.lossless'
		}];

		var webp = webpTests.shift();
		function test(name, uri, cb) {

			var image = new Image();

			function addResult(event) {
				// if the event is from 'onload', check the see if the image's width is
				// 1 pixel (which indiciates support). otherwise, it fails

				var result = event && event.type === 'load' ? image.width == 1 : false;
				var baseTest = name === 'webp';

				/* jshint -W053 */
				addTest(name, baseTest ? new Boolean(result) : result);

				if (cb) cb(event);
			}

			image.onerror = addResult;
			image.onload = addResult;

			image.src = uri;
		}

		// test for webp support in general
		test(webp.name, webp.uri, function(e) {
			// if the webp test loaded, test everything else.
			if (e && e.type === 'load') {
				for (var i = 0; i < webpTests.length; i++) {
					test(webpTests[i].name, webpTests[i].uri);
				}
			}
		});

	});



	var createElement = function() {
		if (typeof document.createElement !== 'function') {
			// This is the case in IE7, where the type of createElement is "object".
			// For this reason, we cannot call apply() as Object is not a Function.
			return document.createElement(arguments[0]);
		} else {
			return document.createElement.apply(document, arguments);
		}
	};

	/*!
	 {
	 "name": "[hidden] Attribute",
	 "property": "hidden",
	 "tags": ["dom"],
	 "notes": [{
	 "name": "WHATWG: The hidden attribute",
	 "href": "http://developers.whatwg.org/editing.html#the-hidden-attribute"
	 }, {
	 "name": "original implementation of detect code",
	 "href": "https://github.com/aFarkas/html5shiv/blob/bf4fcc4/src/html5shiv.js#L38"
	 }],
	 "polyfills": ["html5shiv"],
	 "authors": ["Ron Waldon (@jokeyrhyme)"]
	 }
	 !*/
	/* DOC
	 Does the browser support the HTML5 [hidden] attribute?
	 */

	Modernizr.addTest('hidden', 'hidden' in createElement('a'));

	/*!
	 {
	 "name": "CSS Font rem Units",
	 "caniuse": "rem",
	 "authors": ["nsfmc"],
	 "property": "cssremunit",
	 "tags": ["css"],
	 "builderAliases": ["css_remunit"],
	 "notes": [{
	 "name": "W3C Spec",
	 "href": "http://www.w3.org/TR/css3-values/#relative0"
	 },{
	 "name": "Font Size with rem by Jonathan Snook",
	 "href": "http://snook.ca/archives/html_and_css/font-size-with-rem"
	 }]
	 }
	 !*/

	// "The 'rem' unit ('root em') is relative to the computed
	// value of the 'font-size' value of the root element."
	// you can test by checking if the prop was ditched

	Modernizr.addTest('cssremunit', function() {
		var div = createElement('div');
		try {
			div.style.fontSize = '3rem';
		}
		catch( er ) {}
		return (/rem/).test(div.style.fontSize);
	});

	/*!
	 {
	 "name": "dataset API",
	 "caniuse": "dataset",
	 "property": "dataset",
	 "tags": ["dom"],
	 "builderAliases": ["dom_dataset"],
	 "authors": ["@phiggins42"]
	 }
	 !*/

	// dataset API for data-* attributes
	Modernizr.addTest('dataset', function() {
		var n = createElement('div');
		n.setAttribute('data-a-b', 'c');
		return !!(n.dataset && n.dataset.aB === 'c');
	});

	/*!
	 {
	 "name": "HTML5 Video",
	 "property": "video",
	 "caniuse": "video",
	 "tags": ["html5"],
	 "knownBugs": [
	 "Without QuickTime, `Modernizr.video.h264` will be `undefined`; http://github.com/Modernizr/Modernizr/issues/546"
	 ],
	 "polyfills": [
	 "html5media",
	 "mediaelementjs",
	 "sublimevideo",
	 "videojs",
	 "leanbackplayer",
	 "videoforeverybody"
	 ]
	 }
	 !*/
	/* DOC
	 Detects support for the video element, as well as testing what types of content it supports.

	 Subproperties are provided to describe support for `ogg`, `h264` and `webm` formats, e.g.:

	 ```javascript
	 Modernizr.video         // true
	 Modernizr.video.ogg     // 'probably'
	 ```
	 */

	// Codec values from : github.com/NielsLeenheer/html5test/blob/9106a8/index.html#L845
	//                     thx to NielsLeenheer and zcorpan

	// Note: in some older browsers, "no" was a return value instead of empty string.
	//   It was live in FF3.5.0 and 3.5.1, but fixed in 3.5.2
	//   It was also live in Safari 4.0.0 - 4.0.4, but fixed in 4.0.5

	Modernizr.addTest('video', function() {
		/* jshint -W053 */
		var elem = createElement('video');
		var bool = false;

		// IE9 Running on Windows Server SKU can cause an exception to be thrown, bug #224
		try {
			if ( bool = !!elem.canPlayType ) {
				bool = new Boolean(bool);
				bool.ogg = elem.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,'');

				// Without QuickTime, this value will be `undefined`. github.com/Modernizr/Modernizr/issues/546
				bool.h264 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,'');

				bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,'');

				bool.vp9 = elem.canPlayType('video/webm; codecs="vp9"').replace(/^no$/,'');

				bool.hls = elem.canPlayType('application/x-mpegURL; codecs="avc1.42E01E"').replace(/^no$/,'');
			}
		} catch(e){}

		return bool;
	});

	/*!
	 {
	 "name": "Inline SVG",
	 "property": "inlinesvg",
	 "caniuse": "svg-html5",
	 "tags": ["svg"],
	 "notes": [{
	 "name": "Test page",
	 "href": "http://paulirish.com/demo/inline-svg"
	 }],
	 "polyfills": ["inline-svg-polyfill"]
	 }
	 !*/
	/* DOC
	 Detects support for inline SVG in HTML (not within XHTML).
	 */

	Modernizr.addTest('inlinesvg', function() {
		var div = createElement('div');
		div.innerHTML = '<svg/>';
		return (div.firstChild && div.firstChild.namespaceURI) == 'http://www.w3.org/2000/svg';
	});

	/*!
	 {
	 "name": "script[async]",
	 "property": "scriptasync",
	 "caniuse": "script-async",
	 "tags": ["script"],
	 "builderAliases": ["script_async"],
	 "authors": ["Theodoor van Donge"]
	 }
	 !*/
	/* DOC
	 Detects support for the `async` attribute on the `<script>` element.
	 */

	Modernizr.addTest('scriptasync', 'async' in createElement('script'));

	/*!
	 {
	 "name": "script[defer]",
	 "property": "scriptdefer",
	 "caniuse": "script-defer",
	 "tags": ["script"],
	 "builderAliases": ["script_defer"],
	 "authors": ["Theodoor van Donge"],
	 "warnings": ["Browser implementation of the `defer` attribute vary: http://stackoverflow.com/questions/3952009/defer-attribute-chrome#answer-3982619"],
	 "knownBugs": ["False positive in Opera 12"]
	 }
	 !*/
	/* DOC
	 Detects support for the `defer` attribute on the `<script>` element.
	 */

	Modernizr.addTest('scriptdefer', 'defer' in createElement('script'));


	// isEventSupported determines if the given element supports the given event
	// kangax.github.com/iseventsupported/
	// github.com/Modernizr/Modernizr/pull/636
	//
	// Known incorrects:
	//   Modernizr.hasEvent("webkitTransitionEnd", elem) // false negative
	//   Modernizr.hasEvent("textInput") // in Webkit. github.com/Modernizr/Modernizr/issues/333
	var isEventSupported = (function (undefined) {

		// Detect whether event support can be detected via `in`. Test on a DOM element
		// using the "blur" event b/c it should always exist. bit.ly/event-detection
		var needsFallback = !('onblur' in document.documentElement);

		/**
		 * @param  {string|*}           eventName  is the name of an event to test for (e.g. "resize")
		 * @param  {(Object|string|*)=} element    is the element|document|window|tagName to test on
		 * @return {boolean}
		 */
		function isEventSupportedInner( eventName, element ) {

			var isSupported;
			if ( !eventName ) { return false; }
			if ( !element || typeof element === 'string' ) {
				element = createElement(element || 'div');
			}

			// Testing via the `in` operator is sufficient for modern browsers and IE.
			// When using `setAttribute`, IE skips "unload", WebKit skips "unload" and
			// "resize", whereas `in` "catches" those.
			eventName = 'on' + eventName;
			isSupported = eventName in element;

			// Fallback technique for old Firefox - bit.ly/event-detection
			if ( !isSupported && needsFallback ) {
				if ( !element.setAttribute ) {
					// Switch to generic element if it lacks `setAttribute`.
					// It could be the `document`, `window`, or something else.
					element = createElement('div');
				}

				element.setAttribute(eventName, '');
				isSupported = typeof element[eventName] === 'function';

				if ( element[eventName] !== undefined ) {
					// If property was created, "remove it" by setting value to `undefined`.
					element[eventName] = undefined;
				}
				element.removeAttribute(eventName);
			}

			return isSupported;
		}
		return isEventSupportedInner;
	})();



	// Modernizr.hasEvent() detects support for a given event, with an optional element to test on
	// Modernizr.hasEvent('gesturestart', elem)
	var hasEvent = ModernizrProto.hasEvent = isEventSupported;

	/*!
	 {
	 "name": "Canvas",
	 "property": "canvas",
	 "caniuse": "canvas",
	 "tags": ["canvas", "graphics"],
	 "polyfills": ["flashcanvas", "excanvas", "slcanvas", "fxcanvas"]
	 }
	 !*/
	/* DOC
	 Detects support for the `<canvas>` element for 2D drawing.
	 */

	// On the S60 and BB Storm, getContext exists, but always returns undefined
	// so we actually have to call getContext() to verify
	// github.com/Modernizr/Modernizr/issues/issue/97/
	Modernizr.addTest('canvas', function() {
		var elem = createElement('canvas');
		return !!(elem.getContext && elem.getContext('2d'));
	});

	/*!
	 {
	 "name": "Animated PNG",
	 "async": true,
	 "property": "apng",
	 "tags": ["image"],
	 "builderAliases": ["img_apng"],
	 "notes": [{
	 "name": "Wikipedia Article",
	 "href": "http://en.wikipedia.org/wiki/APNG"
	 }]
	 }
	 !*/
	/* DOC
	 Test for animated png support.
	 */

	Modernizr.addAsyncTest(function () {
		if (!Modernizr.canvas) {
			return false;
		}

		var image = new Image();
		var canvas = createElement('canvas');
		var ctx = canvas.getContext('2d');

		image.onload = function () {
			addTest('apng', function () {
				if (typeof canvas.getContext == 'undefined') {
					return false;
				}
				else {
					ctx.drawImage(image, 0, 0);
					return ctx.getImageData(0, 0, 1, 1).data[3] === 0;
				}
			});
		};

		image.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACGFjVEwAAAABAAAAAcMq2TYAAAANSURBVAiZY2BgYPgPAAEEAQB9ssjfAAAAGmZjVEwAAAAAAAAAAQAAAAEAAAAAAAAAAAD6A+gBAbNU+2sAAAARZmRBVAAAAAEImWNgYGBgAAAABQAB6MzFdgAAAABJRU5ErkJggg==';
	});


	// List of property values to set for css tests. See ticket #21
	var prefixes = (ModernizrProto._config.usePrefixes ? ' -webkit- -moz- -o- -ms- '.split(' ') : []);

	// expose these for the plugin API. Look in the source for how to join() them against your input
	ModernizrProto._prefixes = prefixes;


	/*!
	 {
	 "name": "CSS Calc",
	 "property": "csscalc",
	 "caniuse": "calc",
	 "tags": ["css"],
	 "builderAliases": ["css_calc"],
	 "authors": ["@calvein"]
	 }
	 !*/
	/* DOC
	 Method of allowing calculated values for length units. For example:

	 ```css
	 #elem {
	 width: calc(100% - 3em);
	 }
	 ```
	 */

	Modernizr.addTest('csscalc', function() {
		var prop = 'width:';
		var value = 'calc(10px);';
		var el = createElement('div');

		el.style.cssText = prop + prefixes.join(value + prop);

		return !!el.style.length;
	});

	/*!
	 {
	 "name": "CSS Supports",
	 "property": "supports",
	 "caniuse": "css-featurequeries",
	 "tags": ["css"],
	 "builderAliases": ["css_supports"],
	 "notes": [{
	 "name": "W3 Spec",
	 "href": "http://dev.w3.org/csswg/css3-conditional/#at-supports"
	 },{
	 "name": "Related Github Issue",
	 "href": "github.com/Modernizr/Modernizr/issues/648"
	 },{
	 "name": "W3 Info",
	 "href": "http://dev.w3.org/csswg/css3-conditional/#the-csssupportsrule-interface"
	 }]
	 }
	 !*/

	var newSyntax = 'CSS' in window && 'supports' in window.CSS;
	var oldSyntax = 'supportsCSS' in window;
	Modernizr.addTest('supports', newSyntax || oldSyntax);

	/*!
	 {
	 "name": "CSS Filters",
	 "property": "cssfilters",
	 "caniuse": "css-filters",
	 "polyfills": ["polyfilter"],
	 "tags": ["css"],
	 "builderAliases": ["css_filters"],
	 "notes": [{
	 "name": "MDN article on CSS filters",
	 "href": "https://developer.mozilla.org/en-US/docs/Web/CSS/filter"
	 }]
	 }
	 !*/

	// https://github.com/Modernizr/Modernizr/issues/615
	// documentMode is needed for false positives in oldIE, please see issue above
	Modernizr.addTest('cssfilters', function() {
		var el = createElement('div');
		el.style.cssText = prefixes.join('filter:blur(2px); ');
		if (Modernizr.supports) {
			var supports = 'CSS' in window ?
				window.CSS.supports('filter', 'url()') :
				window.supportsCSS('filter', 'url()');

			// older firefox only supports `url` filters;
			return supports;
		} else {
			return !!el.style.length && ((document.documentMode === undefined || document.documentMode > 9));
		}
	});



	// http://mathiasbynens.be/notes/xhr-responsetype-json#comment-4
	/* istanbul ignore next */
	var testXhrType = function(type) {
		if (typeof XMLHttpRequest == 'undefined') {
			return false;
		}
		var xhr = new XMLHttpRequest();
		xhr.open('get', '/', true);
		try {
			xhr.responseType = type;
		} catch(error) {
			return false;
		}
		return 'response' in xhr && xhr.responseType == type;
	};


	/*!
	 {
	 "name": "XHR responseType='arraybuffer'",
	 "property": "xhrresponsetypearraybuffer",
	 "tags": ["network"],
	 "notes": [{
	 "name": "XMLHttpRequest Living Standard",
	 "href": "http://xhr.spec.whatwg.org/#the-responsetype-attribute"
	 }]
	 }
	 !*/
	/* DOC
	 Tests for XMLHttpRequest xhr.responseType='arraybuffer'.
	 */

	Modernizr.addTest('xhrresponsetypearraybuffer', testXhrType('arraybuffer'));

	/*!
	 {
	 "name": "XHR responseType='blob'",
	 "property": "xhrresponsetypeblob",
	 "tags": ["network"],
	 "notes": [{
	 "name": "XMLHttpRequest Living Standard",
	 "href": "http://xhr.spec.whatwg.org/#the-responsetype-attribute"
	 }]
	 }
	 !*/
	/* DOC
	 Tests for XMLHttpRequest xhr.responseType='blob'.
	 */

	Modernizr.addTest('xhrresponsetypeblob', testXhrType('blob'));

	/*!
	 {
	 "name": "XHR responseType='text'",
	 "property": "xhrresponsetypetext",
	 "tags": ["network"],
	 "notes": [{
	 "name": "XMLHttpRequest Living Standard",
	 "href": "http://xhr.spec.whatwg.org/#the-responsetype-attribute"
	 }]
	 }
	 !*/
	/* DOC
	 Tests for XMLHttpRequest xhr.responseType='text'.
	 */

	Modernizr.addTest('xhrresponsetypetext', testXhrType('text'));


	function getBody() {
		// After page load injecting a fake body doesn't work so check if body exists
		var body = document.body;

		if(!body) {
			// Can't use the real body create a fake one.
			body = createElement('body');
			body.fake = true;
		}

		return body;
	}

	;

	// Inject element with style element and some CSS rules
	function injectElementWithStyles( rule, callback, nodes, testnames ) {
		var mod = 'modernizr';
		var style;
		var ret;
		var node;
		var docOverflow;
		var div = createElement('div');
		var body = getBody();

		if ( parseInt(nodes, 10) ) {
			// In order not to give false positives we create a node for each test
			// This also allows the method to scale for unspecified uses
			while ( nodes-- ) {
				node = createElement('div');
				node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
				div.appendChild(node);
			}
		}

		// <style> elements in IE6-9 are considered 'NoScope' elements and therefore will be removed
		// when injected with innerHTML. To get around this you need to prepend the 'NoScope' element
		// with a 'scoped' element, in our case the soft-hyphen entity as it won't mess with our measurements.
		// msdn.microsoft.com/en-us/library/ms533897%28VS.85%29.aspx
		// Documents served as xml will throw if using &shy; so use xml friendly encoded version. See issue #277
		style = ['&#173;','<style id="s', mod, '">', rule, '</style>'].join('');
		div.id = mod;
		// IE6 will false positive on some tests due to the style element inside the test div somehow interfering offsetHeight, so insert it into body or fakebody.
		// Opera will act all quirky when injecting elements in documentElement when page is served as xml, needs fakebody too. #270
		(!body.fake ? div : body).innerHTML += style;
		body.appendChild(div);
		if ( body.fake ) {
			//avoid crashing IE8, if background image is used
			body.style.background = '';
			//Safari 5.13/5.1.4 OSX stops loading if ::-webkit-scrollbar is used and scrollbars are visible
			body.style.overflow = 'hidden';
			docOverflow = docElement.style.overflow;
			docElement.style.overflow = 'hidden';
			docElement.appendChild(body);
		}

		ret = callback(div, rule);
		// If this is done after page load we don't want to remove the body so check if body exists
		if ( body.fake ) {
			body.parentNode.removeChild(body);
			docElement.style.overflow = docOverflow;
			// Trigger layout so kinetic scrolling isn't disabled in iOS6+
			docElement.offsetHeight;
		} else {
			div.parentNode.removeChild(div);
		}

		return !!ret;

	}

	;

	var testStyles = ModernizrProto.testStyles = injectElementWithStyles;

	/*!
	 {
	 "name": "CSS vh unit",
	 "property": "cssvhunit",
	 "caniuse": "viewport-units",
	 "tags": ["css"],
	 "builderAliases": ["css_vhunit"],
	 "notes": [{
	 "name": "Related Modernizr Issue",
	 "href": "https://github.com/Modernizr/Modernizr/issues/572"
	 },{
	 "name": "Similar JSFiddle",
	 "href": "http://jsfiddle.net/FWeinb/etnYC/"
	 }]
	 }
	 !*/

	testStyles('#modernizr { height: 50vh; }', function( elem ) {
		var height = parseInt(window.innerHeight/2,10);
		var compStyle = parseInt((window.getComputedStyle ?
			getComputedStyle(elem, null) :
			elem.currentStyle)['height'],10);
		Modernizr.addTest('cssvhunit', compStyle == height);
	});

	/*!
	 {
	 "name": "CSS vw unit",
	 "property": "cssvwunit",
	 "caniuse": "viewport-units",
	 "tags": ["css"],
	 "builderAliases": ["css_vwunit"],
	 "notes": [{
	 "name": "Related Modernizr Issue",
	 "href": "https://github.com/Modernizr/Modernizr/issues/572"
	 },{
	 "name": "JSFiddle Example",
	 "href": "http://jsfiddle.net/FWeinb/etnYC/"
	 }]
	 }
	 !*/

	testStyles('#modernizr { width: 50vw; }', function( elem ) {
		var width = parseInt(window.innerWidth / 2, 10);
		var compStyle = parseInt((window.getComputedStyle ?
			getComputedStyle(elem, null) :
			elem.currentStyle).width, 10);

		Modernizr.addTest('cssvwunit', compStyle == width);
	});

	/*!
	 {
	 "name": "Form Validation",
	 "property": "formvalidation",
	 "tags": ["forms", "validation", "attribute"],
	 "builderAliases": ["forms_validation"]
	 }
	 !*/
	/* DOC
	 This implementation only tests support for interactive form validation.
	 To check validation for a specific type or a specific other constraint,
	 the test can be combined:

	 - `Modernizr.inputtypes.number && Modernizr.formvalidation` (browser supports rangeOverflow, typeMismatch etc. for type=number)
	 - `Modernizr.input.required && Modernizr.formvalidation` (browser supports valueMissing)
	 */

	Modernizr.addTest('formvalidation', function() {
		var form = createElement('form');
		if ( !('checkValidity' in form) || !('addEventListener' in form) ) {
			return false;
		}
		if ('reportValidity' in form) {
			return true;
		}
		var invalidFired = false;
		var input;

		Modernizr.formvalidationapi =  true;

		// Prevent form from being submitted
		form.addEventListener('submit', function(e) {
			//Opera does not validate form, if submit is prevented
			if ( !window.opera ) {
				e.preventDefault();
			}
			e.stopPropagation();
		}, false);

		// Calling form.submit() doesn't trigger interactive validation,
		// use a submit button instead
		//older opera browsers need a name attribute
		form.innerHTML = '<input name="modTest" required><button></button>';

		testStyles('#modernizr form{position:absolute;top:-99999em}', function( node ) {
			node.appendChild(form);

			input = form.getElementsByTagName('input')[0];

			// Record whether "invalid" event is fired
			input.addEventListener('invalid', function(e) {
				invalidFired = true;
				e.preventDefault();
				e.stopPropagation();
			}, false);

			//Opera does not fully support the validationMessage property
			Modernizr.formvalidationmessage = !!input.validationMessage;

			// Submit form by clicking submit button
			form.getElementsByTagName('button')[0].click();
		});

		return invalidFired;
	});

	/*!
	 {
	 "name": "Hidden Scrollbar",
	 "property": "hiddenscroll",
	 "authors": ["Oleg Korsunsky"]
	 }
	 !*/
	/* DOC
	 Detects whether scrollbars on overflowed blocks are hidden (a-la iPhone)
	 */

	Modernizr.addTest('hiddenscroll', function() {
		return testStyles('#modernizr {width:100px;height:100px;overflow:scroll}', function(elem) {
			return elem.offsetWidth === elem.clientWidth;
		});
	});


	// adapted from matchMedia polyfill
	// by Scott Jehl and Paul Irish
	// gist.github.com/786768
	var testMediaQuery = (function () {
		var matchMedia = window.matchMedia || window.msMatchMedia;
		if ( matchMedia ) {
			return function ( mq ) {
				var mql = matchMedia(mq);
				return mql && mql.matches || false;
			};
		}

		return function ( mq ) {
			var bool = false;

			injectElementWithStyles('@media ' + mq + ' { #modernizr { position: absolute; } }', function( node ) {
				bool = (window.getComputedStyle ?
						window.getComputedStyle(node, null) :
						node.currentStyle)['position'] == 'absolute';
			});

			return bool;
		};
	})();



	/** Modernizr.mq tests a given media query, live against the current state of the window
	 * A few important notes:
	 * If a browser does not support media queries at all (eg. oldIE) the mq() will always return false
	 * A max-width or orientation query will be evaluated against the current state, which may change later.
	 * You must specify values. Eg. If you are testing support for the min-width media query use:
	 Modernizr.mq('(min-width:0)')
	 * usage:
	 * Modernizr.mq('only screen and (max-width:768)')
	 */
	var mq = ModernizrProto.mq = testMediaQuery;


	/**
	 * contains returns a boolean for if substr is found within str.
	 */
	function contains( str, substr ) {
		return !!~('' + str).indexOf(substr);
	}

	;

	// Helper function for converting kebab-case to camelCase,
	// e.g. box-sizing -> boxSizing
	function cssToDOM( name ) {
		return name.replace(/([a-z])-([a-z])/g, function(str, m1, m2) {
			return m1 + m2.toUpperCase();
		}).replace(/^-/, '');
	}
	;

	/**
	 * Create our "modernizr" element that we do most feature tests on.
	 */
	var modElem = {
		elem : createElement('modernizr')
	};

	// Clean up this element
	Modernizr._q.push(function() {
		delete modElem.elem;
	});



	var mStyle = {
		style : modElem.elem.style
	};

	// kill ref for gc, must happen before
	// mod.elem is removed, so we unshift on to
	// the front of the queue.
	Modernizr._q.unshift(function() {
		delete mStyle.style;
	});



	// Helper function for converting camelCase to kebab-case,
	// e.g. boxSizing -> box-sizing
	function domToCSS( name ) {
		return name.replace(/([A-Z])/g, function(str, m1) {
			return '-' + m1.toLowerCase();
		}).replace(/^ms-/, '-ms-');
	}
	;

	// Function to allow us to use native feature detection functionality if available.
	// Accepts a list of property names and a single value
	// Returns `undefined` if native detection not available
	function nativeTestProps ( props, value ) {
		var i = props.length;
		// Start with the JS API: http://www.w3.org/TR/css3-conditional/#the-css-interface
		if ('CSS' in window && 'supports' in window.CSS) {
			// Try every prefixed variant of the property
			while (i--) {
				if (window.CSS.supports(domToCSS(props[i]), value)) {
					return true;
				}
			}
			return false;
		}
		// Otherwise fall back to at-rule (for Opera 12.x)
		else if ('CSSSupportsRule' in window) {
			// Build a condition string for every prefixed variant
			var conditionText = [];
			while (i--) {
				conditionText.push('(' + domToCSS(props[i]) + ':' + value + ')');
			}
			conditionText = conditionText.join(' or ');
			return injectElementWithStyles('@supports (' + conditionText + ') { #modernizr { position: absolute; } }', function( node ) {
				return getComputedStyle(node, null).position == 'absolute';
			});
		}
		return undefined;
	}
	;

	// testProps is a generic CSS / DOM property test.

	// In testing support for a given CSS property, it's legit to test:
	//    `elem.style[styleName] !== undefined`
	// If the property is supported it will return an empty string,
	// if unsupported it will return undefined.

	// We'll take advantage of this quick test and skip setting a style
	// on our modernizr element, but instead just testing undefined vs
	// empty string.

	// Property names can be provided in either camelCase or kebab-case.

	function testProps( props, prefixed, value, skipValueTest ) {
		skipValueTest = is(skipValueTest, 'undefined') ? false : skipValueTest;

		// Try native detect first
		if (!is(value, 'undefined')) {
			var result = nativeTestProps(props, value);
			if(!is(result, 'undefined')) {
				return result;
			}
		}

		// Otherwise do it properly
		var afterInit, i, propsLength, prop, before;

		// If we don't have a style element, that means
		// we're running async or after the core tests,
		// so we'll need to create our own elements to use
		if ( !mStyle.style ) {
			afterInit = true;
			mStyle.modElem = createElement('modernizr');
			mStyle.style = mStyle.modElem.style;
		}

		// Delete the objects if we
		// we created them.
		function cleanElems() {
			if (afterInit) {
				delete mStyle.style;
				delete mStyle.modElem;
			}
		}

		propsLength = props.length;
		for ( i = 0; i < propsLength; i++ ) {
			prop = props[i];
			before = mStyle.style[prop];

			if (contains(prop, '-')) {
				prop = cssToDOM(prop);
			}

			if ( mStyle.style[prop] !== undefined ) {

				// If value to test has been passed in, do a set-and-check test.
				// 0 (integer) is a valid property value, so check that `value` isn't
				// undefined, rather than just checking it's truthy.
				if (!skipValueTest && !is(value, 'undefined')) {

					// Needs a try catch block because of old IE. This is slow, but will
					// be avoided in most cases because `skipValueTest` will be used.
					try {
						mStyle.style[prop] = value;
					} catch (e) {}

					// If the property value has changed, we assume the value used is
					// supported. If `value` is empty string, it'll fail here (because
					// it hasn't changed), which matches how browsers have implemented
					// CSS.supports()
					if (mStyle.style[prop] != before) {
						cleanElems();
						return prefixed == 'pfx' ? prop : true;
					}
				}
				// Otherwise just return true, or the property name if this is a
				// `prefixed()` call
				else {
					cleanElems();
					return prefixed == 'pfx' ? prop : true;
				}
			}
		}
		cleanElems();
		return false;
	}

	;

	// Modernizr.testProp() investigates whether a given style property is recognized
	// Property names can be provided in either camelCase or kebab-case.
	// Modernizr.testProp('pointerEvents')
	// Also accepts optional 2nd arg, of a value to use for native feature detection, e.g.:
	// Modernizr.testProp('pointerEvents', 'none')
	var testProp = ModernizrProto.testProp = function( prop, value, useValue ) {
		return testProps([prop], undefined, value, useValue);
	};


	// Following spec is to expose vendor-specific style properties as:
	//   elem.style.WebkitBorderRadius
	// and the following would be incorrect:
	//   elem.style.webkitBorderRadius

	// Webkit ghosts their properties in lowercase but Opera & Moz do not.
	// Microsoft uses a lowercase `ms` instead of the correct `Ms` in IE8+
	//   erik.eae.net/archives/2008/03/10/21.48.10/

	// More here: github.com/Modernizr/Modernizr/issues/issue/21
	var omPrefixes = 'Moz O ms Webkit';


	var cssomPrefixes = (ModernizrProto._config.usePrefixes ? omPrefixes.split(' ') : []);
	ModernizrProto._cssomPrefixes = cssomPrefixes;


	/**
	 * atRule returns a given CSS property at-rule (eg @keyframes), possibly in
	 * some prefixed form, or false, in the case of an unsupported rule
	 *
	 * @param prop - String naming the property to test
	 */

	var atRule = function(prop) {
		var length = prefixes.length;
		var cssrule = window.CSSRule;
		var rule;

		if (typeof cssrule === 'undefined') {
			return undefined;
		}

		if (!prop) {
			return false;
		}

		// remove literal @ from beginning of provided property
		prop = prop.replace(/^@/,'');

		// CSSRules use underscores instead of dashes
		rule = prop.replace(/-/g,'_').toUpperCase() + '_RULE';

		if (rule in cssrule) {
			return '@' + prop;
		}

		for ( var i = 0; i < length; i++ ) {
			// prefixes gives us something like -o-, and we want O_
			var prefix = prefixes[i];
			var thisRule = prefix.toUpperCase() + '_' + rule;

			if (thisRule in cssrule) {
				return '@-' + prefix.toLowerCase() + '-' + prop;
			}
		}

		return false;
	};



	var domPrefixes = (ModernizrProto._config.usePrefixes ? omPrefixes.toLowerCase().split(' ') : []);
	ModernizrProto._domPrefixes = domPrefixes;


	// Change the function's scope.
	function fnBind(fn, that) {
		return function() {
			return fn.apply(that, arguments);
		};
	}

	;

	/**
	 * testDOMProps is a generic DOM property test; if a browser supports
	 *   a certain property, it won't return undefined for it.
	 */
	function testDOMProps( props, obj, elem ) {
		var item;

		for ( var i in props ) {
			if ( props[i] in obj ) {

				// return the property name as a string
				if (elem === false) return props[i];

				item = obj[props[i]];

				// let's bind a function
				if (is(item, 'function')) {
					// bind to obj unless overriden
					return fnBind(item, elem || obj);
				}

				// return the unbound function or obj or value
				return item;
			}
		}
		return false;
	}

	;

	/**
	 * testPropsAll tests a list of DOM properties we want to check against.
	 *     We specify literally ALL possible (known and/or likely) properties on
	 *     the element including the non-vendor prefixed one, for forward-
	 *     compatibility.
	 */
	function testPropsAll( prop, prefixed, elem, value, skipValueTest ) {

		var ucProp = prop.charAt(0).toUpperCase() + prop.slice(1),
			props = (prop + ' ' + cssomPrefixes.join(ucProp + ' ') + ucProp).split(' ');

		// did they call .prefixed('boxSizing') or are we just testing a prop?
		if(is(prefixed, 'string') || is(prefixed, 'undefined')) {
			return testProps(props, prefixed, value, skipValueTest);

			// otherwise, they called .prefixed('requestAnimationFrame', window[, elem])
		} else {
			props = (prop + ' ' + (domPrefixes).join(ucProp + ' ') + ucProp).split(' ');
			return testDOMProps(props, prefixed, elem);
		}
	}

	// Modernizr.testAllProps() investigates whether a given style property,
	//     or any of its vendor-prefixed variants, is recognized
	// Note that the property names must be provided in the camelCase variant.
	// Modernizr.testAllProps('boxSizing')
	ModernizrProto.testAllProps = testPropsAll;



	/**
	 * testAllProps determines whether a given CSS property, in some prefixed
	 * form, is supported by the browser. It can optionally be given a value; in
	 * which case testAllProps will only return true if the browser supports that
	 * value for the named property; this latter case will use native detection
	 * (via window.CSS.supports) if available. A boolean can be passed as a 3rd
	 * parameter to skip the value check when native detection isn't available,
	 * to improve performance when simply testing for support of a property.
	 *
	 * @param prop - String naming the property to test (either camelCase or
	 *               kebab-case)
	 * @param value - [optional] String of the value to test
	 * @param skipValueTest - [optional] Whether to skip testing that the value
	 *                        is supported when using non-native detection
	 *                        (default: false)
	 */
	function testAllProps (prop, value, skipValueTest) {
		return testPropsAll(prop, undefined, undefined, value, skipValueTest);
	}
	ModernizrProto.testAllProps = testAllProps;

	/*!
	 {
	 "name": "Flexbox",
	 "property": "flexbox",
	 "caniuse": "flexbox",
	 "tags": ["css"],
	 "notes": [{
	 "name": "The _new_ flexbox",
	 "href": "http://dev.w3.org/csswg/css3-flexbox"
	 }],
	 "warnings": [
	 "A `true` result for this detect does not imply that the `flex-wrap` property is supported; see the `flexwrap` detect."
	 ]
	 }
	 !*/
	/* DOC
	 Detects support for the Flexible Box Layout model, a.k.a. Flexbox, which allows easy manipulation of layout order and sizing within a container.
	 */

	Modernizr.addTest('flexbox', testAllProps('flexBasis', '1px', true));

	/*!
	 {
	 "name": "Flexbox (legacy)",
	 "property": "flexboxlegacy",
	 "tags": ["css"],
	 "polyfills": ["flexie"],
	 "notes": [{
	 "name": "The _old_ flexbox",
	 "href": "http://www.w3.org/TR/2009/WD-css3-flexbox-20090723/"
	 }]
	 }
	 !*/

	Modernizr.addTest('flexboxlegacy', testAllProps('boxDirection', 'reverse', true));

	/*!
	 {
	 "name": "Flexbox (tweener)",
	 "property": "flexboxtweener",
	 "tags": ["css"],
	 "polyfills": ["flexie"],
	 "notes": [{
	 "name": "The _inbetween_ flexbox",
	 "href": "http://www.w3.org/TR/2011/WD-css3-flexbox-20111129/"
	 }],
	 "warnings": ["This represents an old syntax, not the latest standard syntax."]
	 }
	 !*/

	Modernizr.addTest('flexboxtweener', testAllProps('flexAlign', 'end', true));


	// Modernizr.prefixed() returns the prefixed or nonprefixed property name variant of your input
	// Modernizr.prefixed('boxSizing') // 'MozBoxSizing'

	// Properties can be passed as DOM-style camelCase or CSS-style kebab-case.
	// Return values will always be in camelCase; if you want kebab-case, use Modernizr.prefixedCSS().

	// If you're trying to ascertain which transition end event to bind to, you might do something like...
	//
	//     var transEndEventNames = {
	//         'WebkitTransition' : 'webkitTransitionEnd',// Saf 6, Android Browser
	//         'MozTransition'    : 'transitionend',      // only for FF < 15
	//         'transition'       : 'transitionend'       // IE10, Opera, Chrome, FF 15+, Saf 7+
	//     },
	//     transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];

	var prefixed = ModernizrProto.prefixed = function( prop, obj, elem ) {
		if (prop.indexOf('@') === 0) {
			return atRule(prop);
		}

		if (prop.indexOf('-') != -1) {
			// Convert kebab-case to camelCase
			prop = cssToDOM(prop);
		}
		if (!obj) {
			return testPropsAll(prop, 'pfx');
		} else {
			// Testing DOM property e.g. Modernizr.prefixed('requestAnimationFrame', window) // 'mozRequestAnimationFrame'
			return testPropsAll(prop, obj, elem);
		}
	};


	/*!
	 {
	 "name": "Blob URLs",
	 "property": "bloburls",
	 "caniuse": "bloburls",
	 "notes": [{
	 "name": "W3C Working Draft",
	 "href": "http://www.w3.org/TR/FileAPI/#creating-revoking"
	 }],
	 "tags": ["file", "url"],
	 "authors": ["Ron Waldon (@jokeyrhyme)"]
	 }
	 !*/
	/* DOC
	 Detects support for creating Blob URLs
	 */

	var url = prefixed('URL', window, false);
	url = url && window[url];
	Modernizr.addTest('bloburls', url && 'revokeObjectURL' in url && 'createObjectURL' in url);


	// Run each test
	testRunner();

	// Remove the "no-js" class if it exists
	setClasses(classes);

	delete ModernizrProto.addTest;
	delete ModernizrProto.addAsyncTest;

	// Run the things that are supposed to run after the tests
	for (var i = 0; i < Modernizr._q.length; i++) {
		Modernizr._q[i]();
	}

	// Leak Modernizr namespace
	window.Modernizr = Modernizr;


	;

})(window, document);
