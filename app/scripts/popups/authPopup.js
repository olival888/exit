/**
 * Created by Дом on 12.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	let authPopup=document.querySelector(".pop-up_name_auth");
	let authPopupTrigger=authPopup.querySelector("#auth-popup");

	authPopupTrigger.addEventListener("change",(e)=>{
		if(e.target.checked){
			authPopup.querySelector('#auth').checked = true;
			document.body.classList.add("scroll-hidden");

			Array.from(authPopup.querySelectorAll("form")).forEach((form)=>{
				Array.from(form.querySelectorAll("input")).forEach((input)=>{
					let fieldContaner = input.parentElement.parentElement;

					fieldContaner.classList.remove("invalid");
				});

				form.reset();
				form.nextElementSibling.classList.remove("visible");
			});
		}else{
			document.body.classList.remove("scroll-hidden");
		}
	});

	if(!Modernizr.calcviewport){
		var mainHeight=window.innerHeight-190;
		document.querySelector('.pop-up_name_auth .pop-up__block').style.height=mainHeight+'px';
	}
},false);
