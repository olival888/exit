/**
 * Created by Дом on 12.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	let form = document.querySelector(".form_name_auth");
	let formObj = new Form(form, form.nextElementSibling);

	formObj.onsubmit(()=>{
		formObj.simpleSubmitForm()
			.then(()=> {
				if(/.*reservation.*/.test(location.href)){
					chengeInput(document.querySelector('#auth-popup'));
				}else{
					window.location = formObj.response.redirect_to;
				}
			},()=>{
				formObj.markFormInvalid();
			});
	});
});

