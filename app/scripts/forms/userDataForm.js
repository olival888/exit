/**
 * Created by Дом on 13.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	let form = document.querySelector(".form_name_user-data");

	function showChangeBtn(){
		form.querySelector(".form__btn_role_change").style.display = "inline-block";
		form.querySelector(".form__btn_role_send").style.display = "none";
	}

	function showSendBtn(){
		form.querySelector(".form__btn_role_change").style.display = "none";
		form.querySelector(".form__btn_role_send").style.display = "inline-block";

		Array.from(form.querySelectorAll("input")).forEach((input)=>{
			input.removeEventListener("input",showSendBtn);
		});
	}

	Array.from(form.querySelectorAll("input")).forEach((input)=>{
		input.addEventListener("focus",(e)=> {
			Array.from(form.querySelectorAll("input")).forEach((input)=>{
				input.addEventListener("input",showSendBtn);
			});
		});
	});

	form.querySelector(".form__btn_role_change").addEventListener("click",(e)=>{
		e.target.form.querySelector(".form__field-input_name_name").focus();

		showSendBtn();
	});

	let formObj = new Form(form, form.previousElementSibling);

	formObj.onsubmit(()=>{
		formObj.simpleSubmitForm()
			.then(()=> {
				showChangeBtn();
			},()=>{
				formObj.markFormInvalid();
			});
	});
});

