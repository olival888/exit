/**
 * Created by Дом on 12.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	let form = document.querySelector(".form_name_recovery");
	let formObj = new Form(form, form.nextElementSibling);

	formObj.onsubmit(()=>{
		formObj.simpleSubmitForm()
			.then(()=> {
				chengeInput(document.querySelector('#auth-popup'));
			},()=>{
				formObj.markFormInvalid();
			});
	});
});
