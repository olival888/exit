"use strict";

Modernizr.testStyles('#modernizr { height: calc(100vh - 1rem);}', (elem, rule)=>{
  Modernizr.addTest('calcViewport', elem.offsetHeight > 0);
});
