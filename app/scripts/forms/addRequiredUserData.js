/**
 * Created by Дом on 12.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	let form = document.querySelector(".add_required_user_data");
	let formObj = new Form(form, form.nextElementSibling);

	formObj.onsubmit(()=>{
		formObj.simpleSubmitForm()
			.then(()=> {
				window.location = formObj.response.redirect_to;
			},()=>{
				formObj.markFormInvalid();
			});
	});
});
