/**
 * Created by Дом on 13.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	Array.from(document.querySelectorAll("[name='slider-popup']"))
		.forEach((trigger)=>{
		trigger.addEventListener('change',(e)=> {
			let popUpBlock = e.target.parentElement;

			function showPopupBlock() {
				popUpBlock.checked = true;
				popUpBlock.style.zIndex = 100;
			}
			function hidePopupBlock(){
				popUpBlock.checked = false;
				popUpBlock.style.zIndex = -1;
			}

			if (e.target.checked) {
				Promise.resolve()
					.then(()=> {
						//сделать прелоадр
					})
					.then(()=> {
						if (!popUpBlock.querySelector(".slider").classList.contains("initialized")) {
							loadImages(popUpBlock.querySelectorAll(".slider__img"), `/images/media/slides/${popUpBlock.parentElement.querySelector(".quest__img").dataset.src}/`)
								.then(()=> {
									switch (popUpBlock.querySelectorAll(".slider__img").length) {
										case 0:
											popUpBlock.style.display = "none !important";

											break;
										case 1:
											popUpBlock.querySelector(".slider__slide").style.margin = "auto";
											popUpBlock.querySelector(".slider__slide").style.display = "block";

											showPopupBlock();

											break;
										case 2:
											Array.from(popUpBlock.querySelector(".slider__slide"))
												.forEach((slider)=>slider.style.margin = "auto");

											showPopupBlock();

											break;
										default ://оптимизировать
											if (typeof lory === "undefined") {
												loadScripts(["/bower_components/lory/dist/lory.js"],"local")
													.then(()=>{
														popUpBlock.querySelector(".slider").addEventListener('before.lory.init', ()=>{
															showPopupBlock();

															popUpBlock.querySelector(".slider").classList.add("lory-init");
														});

														lory(popUpBlock.querySelector(".slider"), {
															infinite: 3
														});
													})
											}else{
												lory(popUpBlock.querySelector(".slider"), {
													infinite: 3
												});
											}

											break;
									}

									popUpBlock.querySelector(".slider").classList.add("initialized");
								});
						}else{
							showPopupBlock();
						}
					});
			} else {
				hidePopupBlock();
			}
		});
	});
});
