/**
 * Created by Олег on 23.08.2015.
 */


"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	let elem=document.querySelector(".sticky");
	let topOffset=elem.getBoundingClientRect().top;

	window.addEventListener("scroll",(e)=>{
		let scrolled = window.pageYOffset || document.documentElement.scrollTop;

		if(scrolled>=topOffset){
			elem.style.position="fixed";
			elem.nextElementSibling.style.marginTop=elem.offsetHeight+"px";
		}else{
			elem.nextElementSibling.style.marginTop=0;
			elem.style.position="static";
		}
	});
});