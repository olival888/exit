/**
 * Created by Олег on 19.08.2015.
 */

"use strict";

class GenerateSchedule {
	constructor(tableSelector, json) {
		this.tableSelector=tableSelector;
		this.json=json;
	}

	generate(){
		Promise.resolve(this.json)
			.then((json)=>{
				function parseTime(timeStr){
					return parseInt(timeStr.substr(0,2))*60+parseInt(timeStr.substr(3,2));
				}

				function makePriceArr(slot,priceArr){
					if(priceArr[priceArr.length-1].price==slot.price){
						priceArr[priceArr.length-1].duration+=slot.duration;
						priceArr[priceArr.length-1].end_time=slot.end_time;
					}else{
						priceArr.push({
							duration: slot.duration,
							start_time: slot.start_time,
							end_time: slot.end_time,
							price: slot.price,
							quest_id: slot.quest_id
						});
					}

					return priceArr;
				}

				let set=json;

				let normSet={
						quests:[

						]
					};

				set.quests.forEach((quest,questIndex)=>{
					let questSlots=quest.slots;

					let normQuestSlots=[
						{
							duration: parseTime(questSlots[0].start_time),
							start_time: "00:00",
							end_time: questSlots[0].start_time,
							is_reserved: (questSlots[0].start_time=="00:00")?questSlots[0].is_reserved:"true",
							price: (questSlots[0].start_time=="00:00")?questSlots[0].price:0,
							quest_id: questSlots[0].quest_id
						}
					];

					let priceArr=[{
						duration: normQuestSlots[0].duration,
						start_time: "00:00",
						end_time: normQuestSlots[0].start_time,
						price: normQuestSlots[0].price,
						quest_id: questSlots[0].quest_id
					}];

					let questObj={
						id: quest.id,
						title: quest.title,
						count_member: quest.count_member,
						duration: quest.duration,
						normSlots: [],
						normPrice:[]
					};

					normSet.quests.push(questObj);

					quest.slots.forEach((slot,slotIndex)=>{
						if(parseTime(slot.start_time) - parseTime(normQuestSlots[normQuestSlots.length-1].end_time)){
							normQuestSlots.push({
								duration: parseTime(slot.start_time) - parseTime(normQuestSlots[normQuestSlots.length-1].end_time),
								start_time: normQuestSlots[normQuestSlots.length-1].end_time,
								end_time: slot.start_time,
								is_reserved: "true",
								price: 0,
								quest_id: slot.quest_id
							});

							priceArr=makePriceArr(normQuestSlots[normQuestSlots.length-1],priceArr);
						}

						normQuestSlots.push({
							duration: slot.duration,
							start_time:slot.start_time,
							end_time: slot.end_time,
							is_reserved: slot.is_reserved,
							price: parseInt(slot.price),
							quest_id: slot.quest_id,
							id: slot.id
						});

						priceArr=makePriceArr(normQuestSlots[normQuestSlots.length-1],priceArr);
					});

					normSet.quests[questIndex].normSlots=normQuestSlots;
					normSet.quests[questIndex].normPrice=priceArr;
				});

				return Promise.resolve(normSet);
			})
			.then((set)=>{
				set.quests.forEach((quest,questIndex)=>{
						let row=document.createElement("tr");
						row.classList.add("table__row");

						let th=document.createElement("th");
						th.classList.add("table__header-cell");

						let thLink=document.createElement("a");
						thLink.href=`/quest/${quest.id}/`;

						let header=document.createElement("dl");
						header.classList.add("header");

						let headerTitle=document.createElement("dt");
						headerTitle.classList.add("header__title");
						headerTitle.innerText=quest.title;

						let headerDescr=document.createElement("dd");
						headerDescr.classList.add("header__descr");
						headerDescr.innerText=`${quest.count_member} - 4 чел. ${quest.duration} мин.`;

						header.appendChild(headerTitle);
						header.appendChild(headerDescr);

						if(quest.is_private){
							let lock=document.createElement("div");
							headerDescr.classList.add("lock");
							headerDescr.innerText="доступно только после прохождения всех квестов";

							header.appendChild(lock);
						}

						thLink.appendChild(header);
						th.appendChild(thLink);

						row.appendChild(th);

						let infoCell=document.createElement("td");
						infoCell.classList.add("table__info-cell");

						let infoTable=document.createElement("table");
						infoTable.classList.add("info-table");

						let timeRow=document.createElement("tr");
						timeRow.classList.add("info-table__time-row");

						let costRow=document.createElement("tr");
						costRow.classList.add("info-table__cost-row");


						quest.normSlots.forEach((slot,slotIndex)=>{
							let timeCell=document.createElement("td");
							timeCell.classList.add("info-table__time-cell");
							timeCell.setAttribute("colspan",slot.duration/5);

							let timeBlock;
							if(JSON.parse(slot.is_reserved)){
								timeBlock=document.createElement("span");
								timeBlock.classList.add("info-table__time");
								timeBlock.classList.add("info-table__time_state_noactive");
								timeBlock.innerText=(slot.price)?slot.start_time:" ";
							}else{
								timeBlock=document.createElement("a");
								timeBlock.href=`/reservation/${slot.id}/`;
								timeBlock.classList.add("info-table__time");
								timeBlock.classList.add("info-table__time_state_active");
								timeBlock.innerText=slot.start_time;
							}

							timeCell.appendChild(timeBlock);
							timeRow.appendChild(timeCell);
						});

						quest.normPrice.forEach((cost,costIndex)=>{
							let costCell=document.createElement("td");
							costCell.classList.add("info-table__cost-cell");
							costCell.setAttribute("colspan",cost.duration/5);
							if(JSON.parse(cost.price)){
								let costBlock=document.createElement("div");
								costBlock.classList.add("info-table__cost");
								costBlock.innerText=cost.price;

								costCell.appendChild(costBlock);
							}

							costRow.appendChild(costCell);
						});

						infoTable.appendChild(timeRow);
						infoTable.appendChild(costRow);

						infoCell.appendChild(infoTable);

						row.appendChild(infoCell);

						this.tableSelector.appendChild(row);

						this.tableSelector.parentNode.style.height=this.tableSelector.clientHeight+"px";
				});
			});
	}
}