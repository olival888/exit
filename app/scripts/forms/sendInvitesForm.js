/**
 * Created by Дом on 12.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	if(document.querySelectorAll(".form_name_spam").length){
		Array.from(document.querySelectorAll(".form_name_spam")).forEach((form)=>{
			let formObj = new Form(form, form.previousElementSibling);

			formObj.onsubmit(()=>{
				formObj.simpleSubmitForm()
					.then(()=> {
						formObj.form.parentNode.parentNode.parentNode.querySelector(".pop-up__trigger").checked = false;
					},()=>{
						formObj.markFormInvalid();
					});
			});
		});
	}
});
