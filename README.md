# Exit
Десктопная версия сайта **[Exit.am](http://exit.am)**

#### Установка
Необходимо глобально установить Gulp и Bower:

```sh
$ npm install -g gulp bower
```

```sh
$ git clone git@gitlab.com:olival888/exit.git
$ cd exit
$ npm install && bower install
```

Для просмотра проекта:

```sh
$ gulp serve
```

Для сборки пректа:

```sh
$ gulp
```

Так же обязательно должен быть установлен **[ImageMagic](http://www.imagemagick.org/script/binary-releases.php)**

Исходники проекта содержаться в папке **exit/app**

Собраный проект содержится в папке **exit/dist**

#### Страницы
- [Главная](http://localhost:9000)
- [Расписание по датам](http://localhost:9000/date-timetable.html)
- [Расписание квеста](http://localhost:9000/quest-timetable.html)
- [Бронирование квеста](http://localhost:9000/booking.html)
- [Личный кабинет](http://localhost:9000/user-page.html)
- [Контакты](http://localhost:9000/contacts.html)
- [Сертификат](http://localhost:9000/certificate.html)
- [Правила](http://localhost:9000/rules.html)
- [Контакты](http://localhost:9000/contacts.html)

####ToDo данной итерации:

1. Протестить формы и хендлеры (на боевом сервере)
2. Утвердить алгоритм бронирования
3. Генерация таблицы
4. Верстка расписания
5. Верстка лк
6. Перенесение на сервак
7. Перенесение на мобильную версию
8. Протестить новую конфигурацию сборщика

####ToDo следующей итерации:
1. Заменить slick-carusel на другую карусель и избавиться от зависимости jQuery
2. Разобраться с модулями, наследованиями классов, применить их
3. Оптимизировать js скрипты, стили, продумать fallback'и, улучшить кроссбраузерность
4. Покрыть все скрипты тестами.
