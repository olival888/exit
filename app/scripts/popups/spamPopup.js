/**
 * Created by Дом on 14.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	Array.from(document.querySelectorAll(".pop-up_name_spam")).forEach((popup)=>{
		let trigger=popup.querySelector(".pop-up__trigger");

		trigger.addEventListener("change",(e)=>{
			if(e.target.checked){
				trigger.checked = true;

				popup.querySelector("form").reset();
			}else{
				trigger.checked = false;
			}
		});
	});
});
