/**
 * Created by Дом on 10.08.2015.
 */

/*
* продумать вариант без слайдера
* */

document.addEventListener('DOMContentLoaded', ()=>{
	"use strict";

	let questBlock=document.querySelector(".quest_page_timetable");

	Promise.all([
		(new ImgLoad(questBlock.querySelector(".slider__slide:first-child>img"), "/images/media/quests/full")).loadImage(),
		(new ImgLoad(document.querySelector(".quest_position_bottom>.quest__img"), "/images/media/quests/full")).loadImage()
	])
	.then(()=>{
		let slidesArr=questBlock.querySelectorAll(".js_slide:not(:first-child)>img");

		if(slidesArr.length){
			Promise.all([
				loadScripts(["/bower_components/lory/dist/lory.js"],"local"),
				loadImages(questBlock.querySelectorAll(".slider__slide:not(:first-child)>img"),"/images/media/quests/full")
			]).then(()=>{
				let slider=lory(document.querySelector('.slider'), {
					infinite: 1
				});

				setInterval(()=>{
					slider.next();
				},5000);
			});
		}
	});
});

