/**
 * Created by Дом on 10.08.2015.
 */
"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	loadImages(document.querySelectorAll(".quest__img"),"/images/media/quests/full/");


	window.onhashchange = function(e){
		let hashWord=e.newURL.match(/\w+$/)[0];
		Array.from(document.querySelectorAll(".category-list .quest-list")).forEach((questList)=>{
			if(Array.from(questList.querySelectorAll(".quest-list__item"))
					.every((quest)=>quest.classList.contains(hashWord))){
				questList.classList.add("hidden");
			}else{
				Array.from(questList.querySelectorAll(".quest-list__item"))
					.forEach((quest)=>{
						if(quest.classList.contains(hashWord)){
							quest.classList.add("hidden");
						}
					});
			}
		});
	}
});


